# gui-service-go

GUI Service in GoLang

## Project Structure 
[`golang-standards/project-layout`](https://github.com/golang-standards/project-layout)

### `/cmd`

The directory name for each application should match the name of the 
executable you want to have (e.g., `/cmd/myapp`).

### `/internal`

Private application and library code. This is the code you don't want others 
importing in their applications or libraries. Note that this layout pattern is 
enforced by the Go compiler itself. See the Go 1.4 
[`release notes`](https://golang.org/doc/go1.4#internalpackages)

### `/api`

OpenAPI/Swagger specs, JSON schema files, protocol definition files.

### `/configs`

Configuration file templates or default configs.

## Libraries

- [`swaggo/swag`](https://github.com/swaggo/swag)
- [`swaggo/gin-swagger`](https://github.com/swaggo/gin-swagger)
- [`satori/go.uuid`](https://github.com/satori/go.uuid)
- [`google/uuid`](https://github.com/google/uuid)
- [`lib/pq`](https://github.com/lib/pq)
- [`gin-contrib/cors`](https://github.com/gin-contrib/cors)
- [`go-playground/validator.v9`](https://godoc.org/gopkg.in/go-playground/validator.v9)

