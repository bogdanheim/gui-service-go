package db

import "github.com/jinzhu/gorm"

var guiDB *gorm.DB

func GetInstance() *gorm.DB {
	return guiDB
}

func Init() {
	guiDBInstance, err := gorm.Open("postgres", "host=10.10.22.202 port=31696 user=dbuser dbname=gui_db password=passw0rd sslmode=disable")
	if err != nil {
		panic(err)
	}
	guiDB = guiDBInstance
}
