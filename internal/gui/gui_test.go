package gui

import (
	"../model"
	"github.com/google/uuid"
	"testing"
)

func TestConvertGuiToDTO(t *testing.T) {

	var gui = initGui()

	guiDTO, err := convertGuiToDTO(gui)
	if err != nil {
		t.Error(err)
		t.Fail()
	}
	check(t, "ID", gui.Id.String(), *guiDTO.Id, gui.Id.String() == *guiDTO.Id)
	check(t, "VersionID", gui.VersionId.String(), *guiDTO.VersionId, gui.VersionId.String() == *guiDTO.VersionId)
}

func initGui() (gui model.Gui) {
	id, _ := uuid.Parse("c1c920f5-a757-8780-e0c5-491cf905e1f3")
	version, _ := uuid.Parse("e0216c9a-6f81-4d12-93f2-8b5f9bd2c2a6")
	process, _ := uuid.Parse("89606f74-10c9-4d44-9088-33ec61cd22d5")
	code := "interfata_test"
	type1 := "GUI"
	name := "741"
	payload := "{\"id\":\"c1c920f5-a757-8780-e0c5-491cf905e1f3\",\"versionId\":\"e0216c9a-6f81-4d12-93f2-8b5f9bd2c2a6\",\"processDefinitionId\":\"89606f74-10c9-4d44-9088-33ec61cd22d5\",\"code\":\"interfata_test\",\"type\":\"GUI\",\"name\":\"741\",\"description\":\"741\",\"interfaceJson\":{\"components\":{\"DMyLrx6oYSpMJFFsV3Qxn4\":{\"__component\":\"ctrl-textbox\",\"__generics\":{\"code\":\"DMyLrx6oYSpMJFFsV3Qxn4\",\"share\":false,\"display\":true,\"readOnly\":false,\"mandatory\":false,\"sizeDesktop\":0.0,\"sizeTablet\":0.0,\"sizePhone\":0.0,\"padding\":[0.0,0.0,0.0,0.0],\"margin\":[0.0,0.0,0.0,0.0],\"mapLink\":true},\"__events\":{},\"get\":\"m_Clasa855.clasa\",\"set\":\"m_Clasa855.clasa\",\"label\":\"New textbox\",\"align\":\"left\",\"parent\":false},\"YbDRFRMoFERV9GQEjaYf9b\":{\"__component\":\"ctrl-textbox\",\"__generics\":{\"code\":\"YbDRFRMoFERV9GQEjaYf9b\",\"share\":false,\"display\":true,\"readOnly\":false,\"mandatory\":false,\"sizeDesktop\":0.0,\"sizeTablet\":0.0,\"sizePhone\":0.0,\"padding\":[0.0,0.0,0.0,0.0],\"margin\":[0.0,0.0,0.0,0.0],\"mapLink\":true},\"__events\":{},\"get\":\"m_Clasa855.po7\",\"set\":\"m_Clasa855.po7\",\"label\":\"New textbox\",\"align\":\"left\",\"parent\":false}},\"body\":[{\"guid\":\"DMyLrx6oYSpMJFFsV3Qxn4\"},{\"guid\":\"YbDRFRMoFERV9GQEjaYf9b\"}],\"config\":{\"theme\":{\"primary\":\"#027BE3\",\"secondary\":\"#26A69A\",\"tertiary\":\"#555\",\"positive\":\"#21BA45\",\"negative\":\"#DB2828\",\"info\":\"#31CCEC\",\"warning\":\"#F2C037\",\"light\":\"#BDBDBD\",\"faded\":\"#777\",\"dark\":\"#424242\"}}},\"interfaceInputModel\":{\"get\":{\"DMyLrx6oYSpMJFFsV3Qxn4\":\"m_Clasa855.clasa\",\"YbDRFRMoFERV9GQEjaYf9b\":\"m_Clasa855.po7\"},\"save\":{\"DMyLrx6oYSpMJFFsV3Qxn4\":\"m_Clasa855.clasa\",\"YbDRFRMoFERV9GQEjaYf9b\":\"m_Clasa855.po7\"}},\"interfaceTestData\":{}}"
	active := true
	owner, _ := uuid.Parse("807b6c35-70c6-466b-86ce-2b55dad9185f")

	gui = model.Gui{
		Id:                  &id,
		VersionId:           &version,
		ProcessDefinitionId: &process,
		Code:                &code,
		Type:                &type1,
		Name:                &name,
		Description:         &name,
		Payload:             &payload,
		Active:              &active,
		GuiControlRevisions: nil,
		OwnerId:             &owner,
		IsUsed:              nil,
	}
	return gui
}

func check(t *testing.T, varName string, value1 string, value2 string, check bool) {
	t.Logf("=====")
	t.Logf("gui    %v: %v", varName, value1)
	t.Logf("guiDTO %v: %v", varName, value2)
	if check {
		t.Logf("%v check passed!", varName)
	} else {
		t.Errorf("%v check failed!", varName)
		t.Fail()
	}
}

func BenchmarkConvertGuiToDTO(b *testing.B) {
	var gui = initGui()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, err := convertGuiToDTO(gui)
		if err != nil {
			b.Error(err)
			b.Fail()
		}
	}
}
