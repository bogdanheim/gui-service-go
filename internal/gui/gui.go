package gui

import (
	"../db"
	"../model"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/swaggo/swag/example/celler/httputil"
	"net/http"
)

// ListAllGUIsByVersionIdAndProcessDefId godoc
// @Summary List All GUIs By Version ID And Process Definition ID
// @Tags GUI
// @Accept  json
// @Produce  json
// @Param principal header string true "Uncompressed Principal data"
// @Param versionId path string true "Version ID"
// @Param processDefinitionId path string true "Process Definition ID"
// @Success 200 {array} model.GuiDTO
// @Router /gui/version/{versionId}/processDefinition/{processDefinitionId} [get]
func (c *GUIController) ListAllGUIsByVersionIdAndProcessDefId(ctx *gin.Context) {
	versionId := ctx.Param("versionId")
	if len(versionId) == 0 {
		httputil.NewError(ctx, http.StatusBadRequest, fmt.Errorf("versionId cannot be null or empty"))
		return
	}
	processDefinitionId := ctx.Param("processDefinitionId")
	if len(processDefinitionId) == 0 {
		httputil.NewError(ctx, http.StatusBadRequest, fmt.Errorf("processDefinitionId cannot be null or empty"))
		return
	}
	versionIdUUID, err := uuid.Parse(versionId)
	if err != nil {
		httputil.NewError(ctx, http.StatusBadRequest, err)
		return
	}
	processDefinitionIdUUID, err := uuid.Parse(processDefinitionId)
	if err != nil {
		httputil.NewError(ctx, http.StatusBadRequest, err)
		return
	}
	var GUIs []model.Gui
	var guiDTOs []model.GuiDTO
	db.GetInstance().Raw(ListAllGUIsByVersionAndProcessDef, versionIdUUID, processDefinitionIdUUID).Scan(&GUIs)
	for _, gui := range GUIs {
		guiDTOs = append(guiDTOs, model.NewGuiDTO(gui))
	}
	ctx.JSON(http.StatusOK, guiDTOs)
}

// ListAllGUIsByVersionId godoc
// @Summary List All GUIs By Version ID
// @Tags GUI
// @Accept  json
// @Produce  json
// @Param principal header string true "Uncompressed Principal data"
// @Param processDefinitionId path string true "Process Definition ID"
// @Success 200 {array} model.GuiDTO
// @Router /gui/all/version/{versionId} [get]
func (c *GUIController) ListAllGUIsByVersionId(ctx *gin.Context) {
	versionId := ctx.Param("versionId")
	if len(versionId) == 0 {
		httputil.NewError(ctx, http.StatusBadRequest, fmt.Errorf("versionId cannot be null or empty"))
		return
	}
	versionIdUUID, err := uuid.Parse(versionId)
	if err != nil {
		httputil.NewError(ctx, http.StatusBadRequest, err)
		return
	}
	var GUIs []model.Gui
	var guiDTOs []model.GuiDTO
	db.GetInstance().Raw(ListAllGUIsByVersion, versionIdUUID).Scan(&GUIs)
	for _, gui := range GUIs {
		guiDTOs = append(guiDTOs, model.NewGuiDTO(gui))
	}
	ctx.JSON(http.StatusOK, guiDTOs)
}

// GetById godoc
// @Summary Get By ID
// @Tags GUI
// @Accept  json
// @Produce  json
// @Param principal header string true "Uncompressed Principal data"
// @Param id path string true "GUI ID"
// @Success 200 {object} model.GuiDTO
// @Router /gui/one/{id} [get]
func (c *GUIController) GetById(ctx *gin.Context) {
	id := ctx.Param("id")
	if len(id) == 0 {
		httputil.NewError(ctx, http.StatusBadRequest, fmt.Errorf("versionId cannot be null or empty"))
		return
	}

	guiUUID, err := uuid.Parse(id)
	if err != nil {
		httputil.NewError(ctx, http.StatusBadRequest, err)
		return
	}
	var gui model.Gui
	err = db.GetInstance().Table("DEF_GUI").Where("\"ID\" = ?", guiUUID).First(&gui).Error
	if err != nil {
		httputil.NewError(ctx, http.StatusBadRequest, err)
		return
	}
	if gui.Id == nil {
		httputil.NewError(ctx, http.StatusBadRequest, fmt.Errorf("GUI with id %v not found!", id))
		return
	}

	guiDTO, err := convertGuiToDTO(gui)
	if err != nil {
		httputil.NewError(ctx, http.StatusBadRequest, err)
		return
	}
	ctx.JSON(http.StatusOK, guiDTO)
}

func convertGuiToDTO(gui model.Gui) (guiDTO model.GuiDTO, err error) {
	err = json.Unmarshal([]byte(*gui.Payload), &guiDTO)
	if err == nil {

		guiID := gui.Id.String()
		guiDTO.Id = &guiID

		guiDTO.Code = gui.Code
		guiDTO.Name = gui.Name
		guiDTO.Description = gui.Description

		if processDefinitionId := gui.ProcessDefinitionId.String(); processDefinitionId != "" {
			guiDTO.ProcessDefinitionId = &processDefinitionId
		} else {
			guiDTO.ProcessDefinitionId = nil
		}

		if gui.GuiControlRevisions != nil && len(*gui.GuiControlRevisions) > 0 {
			var guiControlRevisions []model.GUIControlRevisionDTO
			for _, guiControlRevision := range *gui.GuiControlRevisions {
				guiControlRevisions = append(guiControlRevisions, model.NewGUIControlRevisionDTO(guiControlRevision))
			}
			guiDTO.GuiControlRevisions = &guiControlRevisions
		}

		guiDTO.UsedOnApps = gui.IsUsed
	}

	return guiDTO, err
}
