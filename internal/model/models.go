package model

import (
	"fmt"
	"github.com/google/uuid"
)

type GuiDTO struct {
	Id                  *string                  `json:"id"`
	VersionId           *string                  `json:"versionId"`
	ProcessDefinitionId *string                  `json:"processDefinitionId"`
	Code                *string                  `json:"code"`
	Type                *string                  `json:"type"`
	Name                *string                  `json:"name"`
	Description         *string                  `json:"description"`
	OwnerId             *string                  `json:"ownerId"`
	UsedOnApps          *bool                    `json:"usedOnApps"`
	InterfaceJson       *map[string]interface{}  `json:"interfaceJson"`
	InterfaceInputModel *InterfaceInputModel     `json:"interfaceInputModel"`
	InterfaceTestData   *map[string]interface{}  `json:"interfaceTestData"`
	ProcessImage        *string                  `json:"processImage"`
	GuiControlRevisions *[]GUIControlRevisionDTO `json:"guiControlRevisions"`
}

type Gui struct {
	Id                  *uuid.UUID            `json:"id" gorm:"column:ID;primary_key:true;type:uuid"`
	VersionId           *uuid.UUID            `json:"versionId" gorm:"column:VERSION_ID;type:uuid"`
	ProcessDefinitionId *uuid.UUID            `json:"processDefinitionId" gorm:"column:PROCESSDEFINITION_ID;type:uuid"`
	Code                *string               `json:"code" gorm:"column:CODE"`
	Type                *string               `json:"type" gorm:"column:TYPE"`
	Name                *string               `json:"name" gorm:"column:NAME"`
	Description         *string               `json:"description" gorm:"column:DESCRIPTION"`
	Payload             *string               `json:"payload" gorm:"column:PAYLOAD"`
	Active              *bool                 `json:"active" gorm:"column:ACTIVE"`
	GuiControlRevisions *[]GUIControlRevision `json:"guiControlRevisions" gorm:"many2many:DEF_GUI_GUICONTROLREVISION;foreignkey:ID;association_foreignkey:ID;association_jointable_foreignkey:GUICONTROLREVISION_ID;jointable_foreignkey:GUI_ID"`
	OwnerId             *uuid.UUID            `json:"ownerId" gorm:"column:OWNER_ID;type:uuid"`
	IsUsed              *bool                 `json:"usedOnApps" gorm:"column:IS_USED"`
}

func NewGuiDTO(gui Gui) (guiDTO GuiDTO) {
	if guiID := gui.Id.String(); guiID != "" {
		guiDTO.Id = &guiID
	}
	if versionId := gui.VersionId.String(); versionId != "" {
		guiDTO.VersionId = &versionId
	}
	if processDefinitionId := gui.ProcessDefinitionId.String(); processDefinitionId != "" {
		guiDTO.ProcessDefinitionId = &processDefinitionId
	}
	guiDTO.Code = gui.Code
	if gui.Type != nil {
		guiDTO.Type = gui.Type
	} else {
		guiType := "GUI"
		guiDTO.Type = &guiType
	}
	guiDTO.Name = gui.Name
	guiDTO.Description = gui.Description
	*guiDTO.OwnerId = gui.OwnerId.String()
	guiDTO.UsedOnApps = gui.IsUsed
	return guiDTO
}

func (s *GuiDTO) validate() (err error) {
	if s.Id == nil || len(*s.Id) == 0 {
		return fmt.Errorf("Id cannot be null or empty!")
	}
	if s.VersionId == nil || len(*s.VersionId) == 0 {
		return fmt.Errorf("VersionId cannot be null or empty!")
	}
	if s.ProcessDefinitionId == nil || len(*s.ProcessDefinitionId) == 0 {
		return fmt.Errorf("ProcessDefinitionId cannot be null or empty!")
	}
	if s.Code == nil || len(*s.Code) == 0 {
		return fmt.Errorf("Code cannot be null or empty!")
	}
	if s.Name == nil || len(*s.Name) == 0 {
		return fmt.Errorf("Name cannot be null or empty!")
	}
	if s.InterfaceJson == nil || len(*s.InterfaceJson) == 0 {
		return fmt.Errorf("Name cannot be null or empty!")
	}
	return nil
}

type InterfaceInputModel struct {
	Get  map[string]interface{} `json:"get"`
	Save map[string]interface{} `json:"save"`
}

type GUIControlRevisionDTO struct {
	GuiControlRevisionId string       `json:"guiControlRevisionId"`
	GuiControlId         string       `json:"guiControlId"`
	Code                 string       `json:"code"`
	Revision             int          `json:"revision"`
	Config               string       `json:"config"`
	Files                []AppfileDTO `json:"files"`
}

func NewGUIControlRevisionDTO(guiControlRevision GUIControlRevision) (guiControlRevisionDTO GUIControlRevisionDTO) {
	if revisionID := guiControlRevision.Id.String(); revisionID != "" {
		guiControlRevisionDTO.GuiControlRevisionId = revisionID
	}
	if controlID := guiControlRevision.GuiControl.Id.String(); controlID != "" {
		guiControlRevisionDTO.GuiControlId = controlID
	}
	guiControlRevisionDTO.Code = guiControlRevision.Code
	guiControlRevisionDTO.Revision = guiControlRevision.Revision
	guiControlRevisionDTO.Config = guiControlRevision.Config
	if len(guiControlRevision.GuiControlRevisionFileSet) > 0 {
		var files []AppfileDTO
		for _, revisionFile := range guiControlRevision.GuiControlRevisionFileSet {
			files = append(files, NewAppfileDTO(revisionFile))
		}
		guiControlRevisionDTO.Files = files
	}
	return guiControlRevisionDTO
}

func NewAppfileDTO(revisionFile GuiControlRevisionFile) (appfileDTO AppfileDTO) {
	if fileID := revisionFile.FileId.String(); fileID != "" {
		appfileDTO.FileUUID = fileID
	}
	appfileDTO.FileType = revisionFile.ContentType
	appfileDTO.FileName = revisionFile.StorageFileName
	return appfileDTO
}

type GUIControlRevision struct {
	Id                        uuid.UUID                `json:"id" gorm:"column:ID;primary_key:true;type:uuid"`
	GuiControl                GUIControl               `json:"guiControl" gorm:"foreignkey:ID;association_foreignkey:GUICONTROL_ID"`
	Code                      string                   `json:"code" gorm:"column:CODE"`
	Revision                  int                      `json:"revision" gorm:"column:REVISION"`
	Config                    string                   `json:"config" gorm:"column:CONFIG"`
	GuiControlRevisionFileSet []GuiControlRevisionFile `json:"guiControlRevisionFileSet" gorm:"foreignkey:GUICONTROLREVISION_ID"`
}

type GuiControlRevisionFile struct {
	Id                 string             `json:"id" gorm:"column:ID;primary_key:true"`
	GuiControlRevision GUIControlRevision `json:"guiControlRevision" gorm:"foreignkey:ID;association_foreignkey:GUICONTROLREVISION_ID"`
	FileId             uuid.UUID          `json:"fileId" gorm:"column:FILEID;type:uuid"`
	ContentType        string             `json:"contentType" gorm:"column:CONTENTTYPE"`
	StorageFileName    string             `json:"storageFileName" gorm:"column:STORAGEFILENAME"`
	Active             bool               `json:"active" gorm:"column:ACTIVE"`
	Type               string             `json:"type" gorm:"column:TYPE"`
}

type GUIControl struct {
	Id                  uuid.UUID            `json:"id" gorm:"column:ID;primary_key:true;type:uuid"`
	Name                string               `json:"name" gorm:"column:NAME"`
	Description         string               `json:"description" gorm:"column:DESCRIPTION"`
	Active              bool                 `json:"active" gorm:"column:ACTIVE"`
	GuiControlRevisions []GUIControlRevision `json:"guiControlRevisions" gorm:"foreignkey:ID;association_foreignkey:GUICONTROL_ID"`
}

type AppfileDTO struct {
	FileUUID string `json:"fileUUID"`
	FileName string `json:"fileName"`
	FileType string `json:"fileType"`
}
