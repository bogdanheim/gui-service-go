package main

import (
	"../../docs"
	"../../internal/db"
	"../../internal/gui"
	"fmt"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"os"
)

var corsConfig cors.Config

func init() {

	// init Swagger info
	fmt.Println("Init Swagger Info...")
	docs.SwaggerInfo.Title = "Gui Service API"
	docs.SwaggerInfo.Description = "This is a sample server Petstore server."
	docs.SwaggerInfo.Version = "1.0"
	docs.SwaggerInfo.Host = "localhost:8080"
	docs.SwaggerInfo.BasePath = "/gui-service"
	docs.SwaggerInfo.Schemes = []string{"http"}

	// init cors config
	fmt.Println("Init Cors Config...")
	corsConfig = cors.DefaultConfig()
	corsConfig.AllowAllOrigins = true
	corsConfig.AllowCredentials = true

	// init Database
	fmt.Println("Init Database...")
	db.Init()
}

func main() {

	hostName, err := os.Hostname()
	if err != nil {
		panic(err)
	}
	fmt.Println("hostname:", hostName)

	defer db.GetInstance().Close()

	runner := gin.Default()
	guiController := gui.NewController()

	v2 := runner.Group("/gui-service")
	{
		gui := v2.Group("/gui")
		{
			gui.GET("/version/:versionId/processDefinition/:processDefinitionId", guiController.ListAllGUIsByVersionIdAndProcessDefId)
			gui.GET("/all/version/:versionId", guiController.ListAllGUIsByVersionId)
			gui.GET("/one/:id", guiController.GetById)
		}

	}
	runner.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	runner.Use(cors.New(corsConfig))
	runner.Run(":8080")
}
